import React, { Component } from "react";

export default class form extends Component {
  render() {
    return (
      <div>
        <form onSubmit={this.props.loadWeather}>
          <input type="text" name="city" placeholder="City..." />
          <input type="text" name="country" placeholder="Country..." />
          <button value="submit">Search</button>
        </form>
      </div>
    );
  }
}
